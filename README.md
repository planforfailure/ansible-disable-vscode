## Disable/Remove VSCode Repository on the RaspberryPi OS 

The latest version of the RaspberryPi OS was shipped with a Microsoft Repository added from the latest push on [Github](https://github.com/RPi-Distro/raspberrypi-sys-mods/issues/41#issuecomment-773220437). Users on [https://www.raspberrypi.org/](https://www.raspberrypi.org/forums/viewtopic.php?f=63&t=301011&p=1810728#p1810728) highlighted their displeasure at adding the Microsoft repsository which showed up in both their lite and Desktop images without any formal notification ahead of the push. VSCode is widely used developers/end users, however many did not like seeing this being added without  prior notification. 

https://www.cyberciti.biz/linux-news/heads-up-microsoft-repo-secretly-installed-on-all-raspberry-pis-linux-os

This playbook removes and sanitises the following from the RaspberryPi OS repositories. 
 - `/etc/apt/sources.d/vscode.list`
 - `/etc/apt/trusted.gpg.d/microsoft.gpg` 

[https://pi-hole.net/](https://pi-hole.net) could be used in place of adding the entry in `/etc/hosts` as part of the requirements.

### Installation
 
 ```bash
 $ git clone https://gitlab.com/planforfailure/ansible-disable-vscode.git
 ```

Edit your [inventory](inventory) file

```bash
ansible-playbook deploy.yml
```


### Alternative Distributions that Support ARM
- https://raspi.debian.net/tested-images/
- https://archlinuxarm.org/
- https://ubuntu.com/download/raspberry-pi
- https://en.opensuse.org/HCL:Raspberry_Pi
- https://www.openbsd.org/arm64.html

